/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
wimb.view = {
    destiniesDropdown: "#destiniesDropdown",
    routesPanel: "#panelRoutes",
    modal: '#myModal',
    panelShedule: "#panelShedule",
    panelBusStops: "#panelBusStops",
    mapContainer: 'map'


};

wimb.view.showMap = function (data)
{
    $.each(data, function (index, element) {

        var myLatLng = {
            lat: parseFloat(element.lat),
            lng: parseFloat(element.lng)
        };

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: element.name + '</br>' + element.estimated_minutes + ' mins.',
            labelContent: element.name + '</br>' + element.estimated_minutes + ' mins.',
            labelAnchor: new google.maps.Point(22, 0),
            labelClass: "labels", // the CSS class for the label
            labelStyle: {opacity: 0.75},
            map: map
        });

        map.setCenter(myLatLng);
    });
};

//Get the selector
wimb.view.getSelector = function (selector)
{
    return $(selector);
};

///Shows th route in the modal
wimb.view.showRouteInModal = function (element)
{
    var modal = wimb.view.getSelector(wimb.view.modal);

    //Init Modal
    modal.modal({
        keyboard: false
    });
    //Init Tabs
    $('#myTabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#rutaNombre').text(element.name);
    $('#rutaSalida').text(element.startName);
    $('#rutaDestino').text(element.destinyName);
    $('#rutaHoraSalida').text(element.shedules[0].departure_hour);
    //TODO: add estimated

    //Fill the lists
    wimb.view.showSheduleList(element.shedules);
    wimb.view.showBusStopsList(element.busstops);
    wimb.view.showMap(element.busstops);
};

//Shows he shedule list in the modal
wimb.view.showSheduleList = function (data) {

    var panelSelector = wimb.view.getSelector(wimb.view.panelShedule);
    panelSelector.empty();

    if (data.length > 0) {

        var list = $('<div>')
                .attr('class', 'list-group');

        panelSelector.append(list);

        $.each(data, function (index, element)
        {
            var viewElement = $('<li>')
                    .attr('class', 'list-group-item')
                    .text('Hora de Salida: ' + element.departure_hour);

            list.append(viewElement);
        });
    } else
    {
        panelSelector.append($('<p>')
                .attr('class', '')
                .text("Ooops! No pudimos encotrar datos"));
    }
};

//Shows he Bus Stops list in the modal
wimb.view.showBusStopsList = function (data) {

    var panelSelector = wimb.view.getSelector(wimb.view.panelBusStops);
    panelSelector.empty();

    if (data.length > 0) {

        var list = $('<div>')
                .attr('class', 'list-group');

        panelSelector.append(list);

        $.each(data, function (index, element)
        {
            var viewElement = $('<li>')
                    .attr('class', 'list-group-item')
                    .text(element.departure_hour);

            list.append(viewElement);

            viewElement
                    .append($('<h4>')
                            .attr('class', 'list-group-item-heading')
                            .text(element.name));

            viewElement
                    .append($('<p>')
                            .attr('class', 'list-group-item-text')
                            .text('Minutos: ' + element.estimated_minutes));
        });
    } else
    {
        panelSelector.append($('<p>')
                .attr('class', '')
                .text("Ooops! No pudimos encotrar datos"));
    }
};

//Build the html for the destinies
wimb.view.displayDestinies = function (data)
{
    var dropdown = wimb.view.getSelector(this.destiniesDropdown);

    //Clean the dropdown
    dropdown.empty();

    $.each(data, function (index, element) {

        dropdown.append(
                $('<li>').append(
                $('<a>')
                .attr('href', '#')
                .append(element.name)
                .click(function () {
                    wimb.controller.destinySelected = element.id;
                    $("#textdropdown").text(element.name);
                })));
    });
};


//Build the html for the destinies
wimb.view.displayRoutes = function (data)
{
    var panelSelector = wimb.view.getSelector(wimb.view.routesPanel);
    panelSelector.empty()

    if (wimb.controller.destinySelected !== -1) {

        if (data.length > 0) {

            var list = $('<div>')
                    .attr('class', 'list-group');

            panelSelector.append(list);

            $.each(data, function (index, element)
            {
                var viewElement = $('<a>')
                        .attr('class', 'list-group-item');

                list.append(viewElement);

                viewElement
                        .append($('<h4>')
                                .attr('class', 'list-group-item-heading')
                                .text(element.name));

                viewElement
                        .append($('<p>')
                                .attr('class', 'list-group-item-text')
                                .text(element.shedules[0].departure_hour));

                viewElement.click(function () {

                    wimb.view.showRouteInModal(element);
                });
            });
        } else
        {
            panelSelector.append($('<p>')
                    .attr('class', '')
                    .text("Ooops! No pudimos enconrar ninguna ruta para el destino que seleccionaste"));
        }
    }
};
