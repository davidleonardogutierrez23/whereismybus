var wimb = wimb || {};

$(document).ready(function () {

    wimb.controller.loadDestinies();

    var btnGetRoute = $("#btnGetRoute");

    btnGetRoute.click(function () {

        wimb.controller.loadRoutes();
    });
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        google.maps.event.trigger(map, "resize");
    });
});

window.initMap = function()
{
    var map_options = {
        center: new google.maps.LatLng(52.516284, 13.377678),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map_canvas = document.getElementById('map');
    map = new google.maps.Map(map_canvas, map_options);
}

