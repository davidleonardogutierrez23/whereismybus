<?php

class RoutesModel extends CI_Model {

    //put your code here

    public function __construct() {

        parent::__construct();

        $this->load->database();
    }

    public function GetDestinies() {

        $query = $this->db->get('busstop');

        return $query->result();
    }

    public function GetAvailableRoutes($destinyId, $time) {

        $routes = $this->db->query
                        ('SELECT r.* 
                            FROM route r
                            WHERE r.id in(
                                SELECT id_route 
                                    FROM route_busstop
                                    WHERE id_busstop = ?)',
                array($destinyId))->result();

        foreach ($routes as $route) {
            
            $startName = $this->db
                    ->get_where('busstop', array('id' => $route->start_id))
                    ->first_row()->name;
            $destinyName = $this->db
                    ->get_where('busstop', array('id' => $route->destiny_id))
                    ->first_row()->name;
            $shedules = $this->db->query
                        ('SELECT s.* 
                            FROM schedule s
                            WHERE s.departure_hour > ?', 
                    array($time))->result();
            
            $busStops = $this->db->query
                        ('SELECT bs.*, rbs.estimated_minutes 
                            FROM busstop bs INNER JOIN route_busstop rbs
                                ON bs.id = rbs.id_busstop 
                                AND rbs.id_route = ?', array($route->id))
                    ->result();
            
            $route->{"startName"} = $startName;
            $route->{"destinyName"} = $destinyName;
            $route->{"busstops"} = $busStops;
            $route->{"shedules"} = $shedules;
        }

        return $routes;
    }

}
