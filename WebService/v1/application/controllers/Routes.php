<?php

class Routes extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->output->set_header("Access-Control-Allow-Origin: http://whereismybus.ga");
        $this->output->set_header("Access-Control-Request-Method: GET");
    }

    public function GetBusStops() {
        $this->load->model('RoutesModel');

        $data = $this->RoutesModel->GetDestinies();

        if (isset($_GET['callback'])) { // Si es una petición cross-domain  
            echo $_GET['callback'] . '(' . json_encode($data) . ')';
        } else {// Si es una normal, respondemos de forma normal  
            echo json_encode($data);
        }
    }

    public function GetAvailableRoutes($destinyId) {
        $this->load->model('RoutesModel');
        
        date_default_timezone_set('America/Costa_Rica');
        $time = date("H:i:s");
        //$time = '18:12:00';
        
        $data = $this->RoutesModel->GetAvailableRoutes($destinyId, $time);

        if (isset($_GET['callback'])) { // Si es una petición cross-domain  
            echo $_GET['callback'] . '(' . json_encode($data) . ')';
        } else { // Si es una normal, respondemos de forma normal  
            echo json_encode($data);
        }
    }

}
