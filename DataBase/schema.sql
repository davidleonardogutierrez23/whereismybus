create table busstop(
	id int(11) NOT NULL AUTO_INCREMENT,
	name varchar(300) NOT NULL,
	lat DOUBLE DEFAULT 0,
	lng DOUBLE DEFAULT 0,
	PRIMARY KEY (id)
);

CREATE TABLE route (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(300) not null,
  start_id int(11) NOT NULL,
  destiny_id int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (destiny_id) 
			REFERENCES busstop(id),
  FOREIGN KEY (start_id) 
			REFERENCES busstop(id)
);

create table schedule(
	id int(11) NOT NULL AUTO_INCREMENT,
	route_id int(11) NOT NULL,
	departure_hour time NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (route_id) 
			REFERENCES route(id)
);

create table route_busstop(
	id_route int(11) NOT NULL,
	id_busstop int(11) NOT NULL,
	estimated_minutes int(11) not null,
	FOREIGN KEY (id_route) 
			REFERENCES route(id),
	FOREIGN KEY (id_busstop) 
			REFERENCES busstop(id)
);

