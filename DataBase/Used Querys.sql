
SELECT bs.*, rbs.estimated_minutes 
FROM busstop bs INNER JOIN route_busstop rbs
ON bs.id = rbs.id_busstop AND rbs.id_route = 1

SELECT r.*
FROM route r
WHERE r.id in(
	SELECT id_route 
	FROM route_busstop
	WHERE id_busstop = 19
);

SELECT s.* 
FROM schedule s
WHERE s.departure_hour > '18:34:00';